var searchData=
[
  ['r_109',['r',['../namespacelab2.html#aa224fa76a61199f2f81095f575a7d574',1,'lab2.r()'],['../namespaceNucleo3.html#ad42f8f1dd538b0f24d850a6019ea70a4',1,'Nucleo3.r()']]],
  ['raw_5fencoder_110',['Raw_Encoder',['../classEncoder_1_1Raw__Encoder.html',1,'Encoder']]],
  ['ref_5frate_111',['ref_rate',['../classlab2_1_1reflex__test.html#aa0c03ed88fbbc35a8737e0899bb26c04',1,'lab2::reflex_test']]],
  ['reflex_5ftest_112',['reflex_test',['../classlab2_1_1reflex__test.html',1,'lab2']]],
  ['refresh_5frate_113',['refresh_rate',['../namespacelab2.html#a0e3f2f4cce5455fb4775d24d392e1b20',1,'lab2']]],
  ['respond_114',['RESPOND',['../classEncoder_1_1Encoder__Interface.html#aec9fa0cedfc52c782444472a36c8c4f4',1,'Encoder::Encoder_Interface']]],
  ['run_115',['run',['../classlab1_1_1vendatron.html#a500399dea6572e5a380bce95107cc4af',1,'lab1.vendatron.run()'],['../classlab2_1_1reflex__test.html#a272436de085b26e09bd73488e30d13d6',1,'lab2.reflex_test.run()'],['../classNucleo3_1_1Nucleo3.html#a4fbe1c214c655bad55510ae6a07de9c8',1,'Nucleo3.Nucleo3.run()'],['../classtermproject_1_1Balance.html#ab1fcc85127e5b97d70984861720ee462',1,'termproject.Balance.run()'],['../classUI3_1_1UI3.html#a07fa3127b237df9784ca6811466ef550',1,'UI3.UI3.run()']]]
];
