var MCP9808_8py =
[
    [ "MCP9808", "classMCP9808_1_1MCP9808.html", "classMCP9808_1_1MCP9808" ],
    [ "address", "MCP9808_8py.html#a40fe4f294ca75fc387f85c69a9239a99", null ],
    [ "baudrate_i2c", "MCP9808_8py.html#ad9d968603bc337378edb43106240f296", null ],
    [ "bus_i2c", "MCP9808_8py.html#a6b730b95d79d14157ad69d338614eaa6", null ],
    [ "i2c", "MCP9808_8py.html#ac88d5dfb505734990bd3fe1d627866ec", null ],
    [ "mcp", "MCP9808_8py.html#a23a70fe3d695d722a4cfffe70882b79e", null ],
    [ "pin_b8", "MCP9808_8py.html#af5e48fd4a1e44aa8b0e28f88fe7d6aab", null ],
    [ "pin_b9", "MCP9808_8py.html#a09ad639a3fc82d0ae25f669146cbfc68", null ],
    [ "temp_c", "MCP9808_8py.html#af494d6163ae41e12198194a6d6f52946", null ],
    [ "temp_f", "MCP9808_8py.html#a5284c2e632037b0ceec442bf2ebab220", null ],
    [ "wait_time", "MCP9808_8py.html#a4f53b0e6084d2951a6352570b4f0dbb2", null ]
];