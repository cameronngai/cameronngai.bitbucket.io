var main4_8py =
[
    [ "address", "main4_8py.html#af82b3c1d8ee0fbf169679dc4246072d6", null ],
    [ "baudrate_i2c", "main4_8py.html#a9d14e27c43c35def3f6052d25de9c95a", null ],
    [ "bus_i2c", "main4_8py.html#aa7d7955a9f4cb1a16184595bcd01f874", null ],
    [ "curr_time", "main4_8py.html#ab3ff63ad21b129595d82c23a3692b1f0", null ],
    [ "i2c", "main4_8py.html#a01bed9afe7377f6f7bd85d37722786c3", null ],
    [ "mcp", "main4_8py.html#ae23ca780709c2ef5b8c6f92fe31fa587", null ],
    [ "mcpTemp", "main4_8py.html#aeae8f815f59a53f884f769ef995ba1d0", null ],
    [ "next_time", "main4_8py.html#ae16663346a91ec501211680a76ce6549", null ],
    [ "pin_b8", "main4_8py.html#ae05cae0cdd36469beacf98cc2b0fb003", null ],
    [ "pin_b9", "main4_8py.html#a1b833299198281c3e60bed7fef8a6b43", null ],
    [ "start_time", "main4_8py.html#a20ebd92dbf69b29838d49a10892ba39e", null ],
    [ "stm", "main4_8py.html#a9a401a582a23e35e4e6070d211f6c874", null ],
    [ "stmTemp", "main4_8py.html#a8705db956055dadc86f443ebd3df15cb", null ],
    [ "time", "main4_8py.html#a7ff3d6fe42f811df8e98dd05b1af6c9f", null ],
    [ "wait_time", "main4_8py.html#add7ca42a89a06cfc473d25835c93da37", null ]
];