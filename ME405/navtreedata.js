/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Cameron Ngai Repository", "index.html", [
    [ "Introduction", "index.html#sec_intro", null ],
    [ "Lab1", "index.html#sec_1", null ],
    [ "Lab2", "index.html#sec_2", null ],
    [ "Lab3", "index.html#sec_3", null ],
    [ "Lab4", "index.html#sec_4", null ],
    [ "Lab5", "index.html#sec_5", null ],
    [ "Lab6", "index.html#sec_6", null ],
    [ "Lab7", "index.html#sec_7", null ],
    [ "Lab8", "index.html#sec_8", null ],
    [ "Lab9", "index.html#sec_9", null ],
    [ "Equations", "Equations.html", [
      [ "Problem Statement", "Equations.html#sec_e1", null ],
      [ "Analysis", "Equations.html#sec_e2", null ]
    ] ],
    [ "simulation", "simulation.html", [
      [ "Introduction", "simulation.html#sec_s0", null ],
      [ "Linear vs. Nonlinear", "simulation.html#sec_s1", null ],
      [ "Simulation 3A", "simulation.html#sec_s2", null ],
      [ "Simulation 3B", "simulation.html#sec_s3", null ],
      [ "Simulation 3C", "simulation.html#sec_s4", null ],
      [ "Simulation 3D", "simulation.html#sec_s5", null ],
      [ "Closed Loop", "simulation.html#sec_s6", null ]
    ] ],
    [ "Packages", "namespaces.html", [
      [ "Packages", "namespaces.html", "namespaces_dup" ],
      [ "Package Functions", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Functions", "namespacemembers_func.html", null ],
        [ "Variables", "namespacemembers_vars.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"Encoder_8py.html",
"main4_8py.html#add7ca42a89a06cfc473d25835c93da37"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';