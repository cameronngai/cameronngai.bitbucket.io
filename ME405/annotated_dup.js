var annotated_dup =
[
    [ "Encoder", "namespaceEncoder.html", "namespaceEncoder" ],
    [ "lab1", "namespacelab1.html", "namespacelab1" ],
    [ "lab2", "namespacelab2.html", "namespacelab2" ],
    [ "MCP9808", "namespaceMCP9808.html", "namespaceMCP9808" ],
    [ "MotorDriver", "namespaceMotorDriver.html", "namespaceMotorDriver" ],
    [ "Nucleo3", "namespaceNucleo3.html", "namespaceNucleo3" ],
    [ "termproject", "namespacetermproject.html", "namespacetermproject" ],
    [ "touch", "namespacetouch.html", "namespacetouch" ],
    [ "UI3", "namespaceUI3.html", "namespaceUI3" ]
];