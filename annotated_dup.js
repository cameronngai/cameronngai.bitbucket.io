var annotated_dup =
[
    [ "ClosedLoop", "namespaceClosedLoop.html", "namespaceClosedLoop" ],
    [ "Elevator", null, [
      [ "ElevatorSim", "classElevator_1_1ElevatorSim.html", "classElevator_1_1ElevatorSim" ],
      [ "TaskElevator", "classElevator_1_1TaskElevator.html", "classElevator_1_1TaskElevator" ],
      [ "User", "classElevator_1_1User.html", "classElevator_1_1User" ]
    ] ],
    [ "Encoder", "namespaceEncoder.html", "namespaceEncoder" ],
    [ "EncoderUI", "namespaceEncoderUI.html", "namespaceEncoderUI" ],
    [ "Lab2", "namespaceLab2.html", "namespaceLab2" ],
    [ "LEDcontrol", "namespaceLEDcontrol.html", "namespaceLEDcontrol" ],
    [ "MotorDriver", "namespaceMotorDriver.html", "namespaceMotorDriver" ],
    [ "NucleoEncoderUI", "namespaceNucleoEncoderUI.html", "namespaceNucleoEncoderUI" ],
    [ "NucleoUI", "namespaceNucleoUI.html", "namespaceNucleoUI" ],
    [ "NucleoUI7", "namespaceNucleoUI7.html", "namespaceNucleoUI7" ],
    [ "UI", "namespaceUI.html", "namespaceUI" ],
    [ "UI7", "namespaceUI7.html", "namespaceUI7" ]
];