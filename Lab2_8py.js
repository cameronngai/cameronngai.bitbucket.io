var Lab2_8py =
[
    [ "Task_1", "classLab2_1_1Task__1.html", "classLab2_1_1Task__1" ],
    [ "Task_2", "classLab2_1_1Task__2.html", "classLab2_1_1Task__2" ],
    [ "Normal_Periodic_Pattern", "classLab2_1_1Normal__Periodic__Pattern.html", "classLab2_1_1Normal__Periodic__Pattern" ],
    [ "func_1", "Lab2_8py.html#a5984159ab2e111c4fc258475aaca8646", null ],
    [ "func_2", "Lab2_8py.html#a2354638f285fae60be41976f885e79f1", null ],
    [ "interval1", "Lab2_8py.html#a9a57d1182860be98f83a89cd59d5e1da", null ],
    [ "interval2", "Lab2_8py.html#ad8d0593365489b7f4e03d4491bfc3b86", null ],
    [ "led", "Lab2_8py.html#a55f1e5117ad087f6945e678acb7d7a6e", null ],
    [ "pattern_1", "Lab2_8py.html#af8825ba50d8ff3dc9cf3dfc8945abeec", null ],
    [ "pattern_2", "Lab2_8py.html#a3583bbf47713a9e4715e7f38497ec695", null ],
    [ "period", "Lab2_8py.html#a8cd2bc57cd2164d042aa488a3467223a", null ],
    [ "pin5", "Lab2_8py.html#a69ac548a9b588f6a382be057e274c055", null ],
    [ "refresh_rate", "Lab2_8py.html#a2b47c634c8ee918b153712580f58ce63", null ],
    [ "stop_time", "Lab2_8py.html#ac8ceeb438027d91f5ea3a475334cb48c", null ],
    [ "t", "Lab2_8py.html#aef8c2fb2f46fa7a012ffd89b6f7b511d", null ],
    [ "t1", "Lab2_8py.html#a87c0e06fe8525d97952a22a0b80bdb96", null ],
    [ "t2", "Lab2_8py.html#a7474aa5125a1592bedafed196dd58f70", null ]
];