var searchData=
[
  ['get_5fcommand_241',['get_command',['../classEncoderUI_1_1Encoder__UI.html#a9212a84c7826dff4886feb2e1e23e329',1,'EncoderUI.Encoder_UI.get_command()'],['../classLEDcontrol_1_1blink__interface.html#abfd18834d8faed2d496b073c8e03e8ba',1,'LEDcontrol.blink_interface.get_command()'],['../classNucleoEncoderUI_1_1NucleoUI.html#a5aa11cd22875b8729f34df4ad94abd58',1,'NucleoEncoderUI.NucleoUI.get_command()'],['../classNucleoUI_1_1NucleoUI.html#ae9f6ed61fbb52f27e71f37d4b713bf09',1,'NucleoUI.NucleoUI.get_command()'],['../classNucleoUI7_1_1NucleoUI.html#a36ed1aad0ced59d29fdb83451edfe1dd',1,'NucleoUI7.NucleoUI.get_command()'],['../classUI_1_1UI.html#ae3de7998f64bfd028010433bf2008635',1,'UI.UI.get_command()'],['../classUI7_1_1UI.html#af71ea3364b024ef0cc9a9f6507665f50',1,'UI7.UI.get_command()']]],
  ['get_5fkp_242',['get_Kp',['../classClosedLoop_1_1ClosedLoop.html#a38ef1cd742f356fcfad3e999b91e6242',1,'ClosedLoop::ClosedLoop']]],
  ['get_5fnumb_243',['get_numb',['../classNucleoUI_1_1NucleoUI.html#a130ddd57113c298319e19fec6c7eca4e',1,'NucleoUI.NucleoUI.get_numb()'],['../classNucleoUI7_1_1NucleoUI.html#acec0226dfde5eea4beb955d53a108fa0',1,'NucleoUI7.NucleoUI.get_numb()']]],
  ['get_5fposition_244',['get_position',['../classEncoder_1_1Raw__Encoder.html#ad27b81e56034d85f28ac3cb57f743532',1,'Encoder.Raw_Encoder.get_position()'],['../classEncoder_1_1Encoder.html#a756e2d8ed0343f3909fd2665d9b56331',1,'Encoder.Encoder.get_position()']]],
  ['get_5fv_245',['get_v',['../classUI7_1_1UI.html#a1415fdd971d6bc71d341531124879d94',1,'UI7::UI']]]
];
