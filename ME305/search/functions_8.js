var searchData=
[
  ['send_5farray_251',['send_array',['../classNucleoEncoderUI_1_1NucleoUI.html#a7a122edf7d5247388dc8e600b74580cc',1,'NucleoEncoderUI.NucleoUI.send_array()'],['../classNucleoUI_1_1NucleoUI.html#a3811b4bc7edc4a48a980922e3c2c7439',1,'NucleoUI.NucleoUI.send_array()'],['../classNucleoUI7_1_1NucleoUI.html#a83ebb08c71ccc125b52afbc7a08e10cf',1,'NucleoUI7.NucleoUI.send_array()'],['../classUI7_1_1UI.html#adb0584b81bce3388e57b9e9e9d8328ee',1,'UI7.UI.send_array()']]],
  ['send_5fcommand_252',['send_command',['../classEncoderUI_1_1Encoder__UI.html#adcb35b1a89e944e72cd1ff64b27ee264',1,'EncoderUI.Encoder_UI.send_command()'],['../classUI_1_1UI.html#ac31c3290433d68a26f516a1d3b6f732e',1,'UI.UI.send_command()'],['../classUI7_1_1UI.html#af699ba1aff1140202df7c00e96424790',1,'UI7.UI.send_command()']]],
  ['set_5fkp_253',['set_Kp',['../classClosedLoop_1_1ClosedLoop.html#ad7c23cefb90beab56623c28b0b0ca979',1,'ClosedLoop::ClosedLoop']]],
  ['set_5fposition_254',['set_position',['../classEncoder_1_1Raw__Encoder.html#a95bbc3b221d532940741d8e438109982',1,'Encoder::Raw_Encoder']]],
  ['set_5fvel_255',['set_vel',['../classClosedLoop_1_1ClosedLoop.html#a168c5631da01fb37699a7ba929311fe1',1,'ClosedLoop::ClosedLoop']]],
  ['stop_5fcollection_256',['stop_collection',['../classEncoderUI_1_1Encoder__UI.html#a9196ba0888dfc06b297624827c07eef7',1,'EncoderUI.Encoder_UI.stop_collection()'],['../classUI_1_1UI.html#a13452366e48e6e1381a1cf80d67fc3d0',1,'UI.UI.stop_collection()'],['../classUI7_1_1UI.html#a67bbd2adc5fe3b9f53d7698ae98c33c4',1,'UI7.UI.stop_collection()']]]
];
