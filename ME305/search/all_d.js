var searchData=
[
  ['n_96',['n',['../classLEDcontrol_1_1blink__interface.html#abbd461ffdd589aa332bd14903c357162',1,'LEDcontrol::blink_interface']]],
  ['next_5ftime_97',['next_time',['../classElevator_1_1TaskElevator.html#aed6848bee6188f795adf104d7aece51c',1,'Elevator.TaskElevator.next_time()'],['../classElevator_1_1ElevatorSim.html#a83e16ee2445c47a8a8118951ad776100',1,'Elevator.ElevatorSim.next_time()'],['../classEncoder_1_1Encoder.html#a081541a5ec3fbd611dce4b57f50d4d5b',1,'Encoder.Encoder.next_time()'],['../classEncoder_1_1Encoder__Interface.html#ab1bada244cc6d534141e5a55f8ed4302',1,'Encoder.Encoder_Interface.next_time()'],['../classEncoderUI_1_1Encoder__UI.html#a58b187ed98b63ffc297375e38426a095',1,'EncoderUI.Encoder_UI.next_time()'],['../classLab2_1_1Task__1.html#af578b253d07bb2e616228b9bcc99e6c7',1,'Lab2.Task_1.next_time()'],['../classLab2_1_1Task__2.html#a070510401a6c4da67c9548a62b74554b',1,'Lab2.Task_2.next_time()'],['../classLab2_1_1Normal__Periodic__Pattern.html#a2c4fc3f765518f892d4db5b296668f3c',1,'Lab2.Normal_Periodic_Pattern.next_time()'],['../classNucleoUI7_1_1NucleoUI.html#a5059a3db42a35c1f14ac391fd5ba0246',1,'NucleoUI7.NucleoUI.next_time()'],['../classUI_1_1UI.html#a495c8a06fc549836e5ce948d5f4b58f8',1,'UI.UI.next_time()'],['../classUI7_1_1UI.html#ab7b6cba7faba7215f223467980ea7e30',1,'UI7.UI.next_time()']]],
  ['no_5ffreq_98',['NO_FREQ',['../classLEDcontrol_1_1blink.html#ac26970306710a99e2500e7e51460019c',1,'LEDcontrol::blink']]],
  ['normal_5fperiodic_5fpattern_99',['Normal_Periodic_Pattern',['../classLab2_1_1Normal__Periodic__Pattern.html',1,'Lab2']]],
  ['not_5fon_100',['NOT_ON',['../classElevator_1_1User.html#a5f0182bf1ee1db077086a081f90aa3b4',1,'Elevator::User']]],
  ['nucleoencoderui_101',['NucleoEncoderUI',['../namespaceNucleoEncoderUI.html',1,'']]],
  ['nucleoencoderui_2epy_102',['NucleoEncoderUI.py',['../NucleoEncoderUI_8py.html',1,'']]],
  ['nucleoui_103',['NucleoUI',['../classNucleoUI7_1_1NucleoUI.html',1,'NucleoUI7.NucleoUI'],['../classNucleoUI_1_1NucleoUI.html',1,'NucleoUI.NucleoUI'],['../classNucleoEncoderUI_1_1NucleoUI.html',1,'NucleoEncoderUI.NucleoUI'],['../namespaceNucleoUI.html',1,'NucleoUI']]],
  ['nucleoui_2epy_104',['NucleoUI.py',['../NucleoUI_8py.html',1,'']]],
  ['nucleoui7_105',['NucleoUI7',['../namespaceNucleoUI7.html',1,'']]],
  ['nucleoui7_2epy_106',['NucleoUI7.py',['../NucleoUI7_8py.html',1,'']]],
  ['null_5fchar_107',['NULL_CHAR',['../classEncoder_1_1Encoder__Interface.html#a6b31a5378389ab96a3ce47a5548e5448',1,'Encoder::Encoder_Interface']]],
  ['num_5fpatterns_108',['num_patterns',['../classLab2_1_1Task__2.html#a2a877cc2b9170da2bba909a00058763a',1,'Lab2::Task_2']]],
  ['numb_109',['numb',['../classUI_1_1UI.html#a441b6b07b3abfd6fc31501974a227f9a',1,'UI.UI.numb()'],['../classUI7_1_1UI.html#ae7d6df513e21d3c30714733a8aee1531',1,'UI7.UI.numb()']]]
];
