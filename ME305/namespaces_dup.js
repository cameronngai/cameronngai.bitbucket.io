var namespaces_dup =
[
    [ "ClosedLoop", "namespaceClosedLoop.html", null ],
    [ "Encoder", "namespaceEncoder.html", null ],
    [ "Encoder_Test", "namespaceEncoder__Test.html", null ],
    [ "EncoderUI", "namespaceEncoderUI.html", null ],
    [ "fibonacci", "namespacefibonacci.html", null ],
    [ "Lab2", "namespaceLab2.html", null ],
    [ "LEDcontrol", "namespaceLEDcontrol.html", null ],
    [ "MotorDriver", "namespaceMotorDriver.html", null ],
    [ "NucleoEncoderUI", "namespaceNucleoEncoderUI.html", null ],
    [ "NucleoUI", "namespaceNucleoUI.html", null ],
    [ "NucleoUI7", "namespaceNucleoUI7.html", null ],
    [ "UI", "namespaceUI.html", null ],
    [ "UI7", "namespaceUI7.html", null ]
];