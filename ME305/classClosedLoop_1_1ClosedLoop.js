var classClosedLoop_1_1ClosedLoop =
[
    [ "__init__", "classClosedLoop_1_1ClosedLoop.html#af5c30b43302c7b268081af69da5a91e6", null ],
    [ "get_Kp", "classClosedLoop_1_1ClosedLoop.html#a38ef1cd742f356fcfad3e999b91e6242", null ],
    [ "run", "classClosedLoop_1_1ClosedLoop.html#a91dd769148fcfa45e97aafd8333f673d", null ],
    [ "set_Kp", "classClosedLoop_1_1ClosedLoop.html#ad7c23cefb90beab56623c28b0b0ca979", null ],
    [ "set_vel", "classClosedLoop_1_1ClosedLoop.html#a168c5631da01fb37699a7ba929311fe1", null ],
    [ "act_vel", "classClosedLoop_1_1ClosedLoop.html#a2107bd9f7272d613878c65b08aa861fc", null ],
    [ "curr_time", "classClosedLoop_1_1ClosedLoop.html#ab7e9644531a05c08be4f28182f8048fd", null ],
    [ "des_vel", "classClosedLoop_1_1ClosedLoop.html#a1b20cf0b83d259939c060ce708bf71d1", null ],
    [ "encoder", "classClosedLoop_1_1ClosedLoop.html#a405797effb587c18c2ae2cd9f9911f85", null ],
    [ "init_time", "classClosedLoop_1_1ClosedLoop.html#ac9349a7a391a8d0b5b05c0f48a8ef004", null ],
    [ "Kp", "classClosedLoop_1_1ClosedLoop.html#a600c2ee167190f2afdf9fc38c849cf01", null ],
    [ "motor", "classClosedLoop_1_1ClosedLoop.html#a904cb2bb390bf5876c4e8572564c6ffe", null ],
    [ "rate", "classClosedLoop_1_1ClosedLoop.html#a0bb3007ead16629eaea74e4a10a08687", null ],
    [ "refresh_time", "classClosedLoop_1_1ClosedLoop.html#ab6da4ba489a57ba531f8932b2c95ada4", null ],
    [ "uart", "classClosedLoop_1_1ClosedLoop.html#a9e7f2c84a8add0d7bf1dda7d7317f4b9", null ],
    [ "Vdc", "classClosedLoop_1_1ClosedLoop.html#aa11bc76678a6648ec1e02d3d6b619c3d", null ]
];